<?php

return [

    'name' => 'Multimedia',
    'email' => 'info@example.com',
    'version' => 'v5.8.13',
    'pages' => '0',
    'groups' => '0',
    'secure_folders' => 'secure',
    'storage_path' => 'app/coaster'

];
