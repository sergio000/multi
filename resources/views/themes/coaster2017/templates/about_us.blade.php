{!! PageBuilder::section('head') !!}
    <div class="">

        <div class="row margen">
            <div class="col-md-2"></div>
            <div class="col-md-5">
                <div class="imagen">
                    {!! PageBuilder::block('image_code') !!}
                </div>
                <p>
                    {!! PageBuilder::block('title_about') !!}
                </p>
                <p>
                    {!! PageBuilder::block('title_about2') !!}

                </p>
               <p class="margin-text">
                   {!! PageBuilder::block('description_about') !!}
               </p>

            </div>
            <div class="col-md-5">
                {!! PageBuilder::block('imagen') !!}

            </div>

        </div>

    </div>
<div>
    {!! PageBuilder::block('social') !!}
</div>

{!! PageBuilder::section('footer') !!}