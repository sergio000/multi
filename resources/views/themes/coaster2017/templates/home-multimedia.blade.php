{!! PageBuilder::section('head') !!}

<div class="row margen">
    <div class="col-md-3">
        <div class="boton-redondo"><a href="#"><span style="color: #0e0d0d; ">-</span></a></div><span class="color-blanco">Close</span>
</div>
    <div class="col-md-5">
        {!! PageBuilder::menu('main_menu') !!}
    </div>
    <div class="col-md-4">
        <div class="margin-texto">
            <p class="color-blanco">{{ PageBuilder::block('texto1') }}</p>
            <p class="color-blanco">{{ PageBuilder::block('descripcion') }}</p>
            <button class="btn btn-success boton-email">E-Mail</button>
            <div>
                {!! PageBuilder::block('social') !!}
            </div>
        </div>
    </div>
</div>

<style>
    html{
        background-image: radial-gradient(at center center, rgb(99, 99, 99) 0%, rgb(6, 14, 15) 100%);
        background-repeat: no-repeat;
        min-height: 100%;
    }
    body{
        background: none;
    }

</style>






