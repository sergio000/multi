@if ($is_first || $count % 4 == 1)
        @endif
                <span class="color-blanco">
				<a {!! PageBuilder::block('social_link') !!}>
                        {!! PageBuilder::block('social_icon') !!} &nbsp; {{ PageBuilder::block('social_name') }}
                    </a>
                </span>

@if ($is_last || $count % 4 == 0)

@endif